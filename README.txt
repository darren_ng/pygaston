
Gaston Graph Mining with Python
This is a python implementation of the Gaston graph mining algorithm.



Software Requirements:
 - python 3.2 or later
 - networkx 1.11
 - matplotlib
 - Redis
 Installation:
 `cd gaston_py`
 `pip install .`

Follow this link to install `Redis`:
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04

Also, please edit your redis configuration on the file `redis.py` 
Command Line Interface:
For usage instructions, use the following command.
`gaston -h`

Examples:
`python gaston 0.95 test_files/medium_chemical.txt -o output_files/ -c -t`
`python gaston 6 test_files/medium_chemical.txt`
`python gaston 2 test_files/Chemical_340.txt`

Notes: 
 - Support is defined as frequency(subgraph) / count(graphs). See reference [1] below for details.
 - If an output directory is provided: 
     * Frequent subgraphs are drawn using matplotlib and saved under [output folder]/graphs/.
     * A `line_graph.txt` file is generated containing the frequent subgraphs in Line Graph format.
 - Test files are available in the `test_files` directory.  The Chemical_340 dataset was obtained 
     from Nijssen and Kok's website.

Run Tests:
`python setup.py test`

Performance:
 - This Python implementation is significantly slower than the original implementation 
      in C++ by Nijssen and Kok, as well as the Java implementation by the ParSeMis library.
 - It is also much slower than gSpan impelementations in Python.  This is likely due
      to the use of an occurrence list, which tracks all subgraphs that have been seen to 
      ensure the correctness of the algorithm.

References:
[1] Siegfried Nijssen and Joost Kok. A Quickstart in Frequent Structure Mining Can 
  Make a Difference. Proceedings of the SIGKDD, 2004.
http://liacs.leidenuniv.nl/~nijssensgr/gaston/index.html

Additional Resources:
Java implementation of the Gaston algorithm
https://www2.cs.fau.de/EN/research/zold/ParSeMiS/index.html
https://github.com/timtadh/parsemis
